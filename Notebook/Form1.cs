﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notebook
{
    public partial class Form1 : Form
    {
        private int tabID = 1;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        // Добавляем новую вкладку
        private void AddNewTab() 
        {
            TabPage tab = new TabPage("new" + tabID);
            RichTextBox tb = new RichTextBox();
            tb.Dock = DockStyle.Fill;
            tabControl1.TabPages.Add(tab);
            tab.Controls.Add(tb);
            tabControl1.SelectedTab = tab;
            tabID += 1;
        }
        // Добавляем новую вкладку c текстом из файла
        private void AddContentTab(string name, string content)
        {
            TabPage tab = new TabPage(name);
            RichTextBox tb = new RichTextBox();
            tb.Dock = DockStyle.Fill;
            tb.Text = content;
            tabControl1.TabPages.Add(tab);
            tab.Controls.Add(tb);
            tabControl1.SelectedTab = tab;
        }
        //
        private void Cut() 
        {
            RichTextBox rtb = null;
            if (tabControl1.TabCount != 0) 
            {
                rtb = tabControl1.SelectedTab.Controls[0] as RichTextBox;
                rtb.Cut();
            }
        }
        private void Copy()
        {
            RichTextBox rtb = null;
            if (tabControl1.TabCount != 0)
            {
                rtb = tabControl1.SelectedTab.Controls[0] as RichTextBox;
                rtb.Copy();
            }
        }
        private void Past()
        {
            RichTextBox rtb = null;
            if (tabControl1.TabCount != 0)
            {
                rtb = tabControl1.SelectedTab.Controls[0] as RichTextBox;
                rtb.Paste();
            }
        }
        //
        private void OpenFile() 
        {
            Stream openStream;
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                if ((openStream = ofd.OpenFile()) != null)
                {
                    string strFileName = ofd.FileName;
                    string fileText = File.ReadAllText(strFileName);

                    AddContentTab(strFileName, fileText);
                }
            }
        }
        private void SaveFile() 
        {
            //TabPage tab;
            //TextWriter file = new StreamWriter("C:\\demo\\demo.txt");
            //file.Write(tabControl1.TabPages.);
            //file.Close(); 
        }
        private void SaveAsFile()
        {
            Stream saveStream;
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if ((saveStream = sfd.OpenFile()) != null)
                {
                    string strFileName = sfd.FileName;
                    string fileText = File.ReadAllText(strFileName);

                }
            }
        }
        //
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewTab();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddNewTab();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Copy();
        }

        private void pastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Past();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Copy();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Cut();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Past();
        }

        private void sveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAsFile();
        }
    }
}
